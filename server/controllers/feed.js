const fs = require('fs');
const path = require('path');

const { validationResult } = require('express-validator');

const io = require('../socket');
const Post = require('../model/Post');
const User = require('../model/User');

const POSTS_PER_PAGE = 5;

exports.getPosts = async (req, res, next) => {
    try {
        const page = req.query.page || 1;
        const totalItems = await Post.find().countDocuments();
        const posts = await Post.find().populate('creator', '-password -posts').sort({ createdAt: -1 }).skip((page - 1) * POSTS_PER_PAGE).limit(POSTS_PER_PAGE);

        return res.status(200).json({
            message: 'Fetched posts successfult!',
            posts,
            totalItems
        });
    } catch (ex) {
        if (!ex.statusCode) {
            ex.statusCode = 500;
        }

        next(ex);
    }
};

exports.getPost = async (req, res, next) => {
    try {
        const { postId } = req.params;
        const post = await Post.findById(postId).populate('creator', '-password -posts');

        if (!post) {
            const error = new Error('Post with given id not found!');
            error.statusCode = 404;

            throw error;
        }

        res.status(200).json({
            post
        });
    } catch (ex) {
        if (!ex.statusCode) {
            ex.statusCode = 500;
        }

        next(ex);
    }
};

exports.postPost = async (req, res, next) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            const error = new Error('Validation failed, entered data is incorrect!');
            error.statusCode = 422;

            throw error;
        }

        if (!req.file) {
            const error = new Error('No image provided!');
            error.statusCode = 422;

            throw error;
        }

        const imageUrl = req.file.path;
        const { title, content } = req.body;
        const post = await new Post({
            title,
            content,
            imageUrl,
            creator: req.userId
        }).save();

        const user = await User.findById(req.userId);
        user.posts.push(post);
        await user.save();

        io.getIO().emit('posts', {
            action: 'create',
            post: {
                ...post._doc,
                creator: {
                    _id: user.id,
                    name: user.name
                }
            }
        });

        res.status(201).json({
            message: 'Post created successfully!',
            post,
            creator: {
                _id: user._id,
                name: user.name
            }
        })
    } catch (ex) {
        if (!ex.statusCode) {
            ex.statusCode = 500;
        }

        next(ex);
    }
};

exports.putPost = async (req, res, next) => {
    try {
        const erros = validationResult(req);

        if (!erros.isEmpty()) {
            const error = new Error('Validation failed, entered data is incorrect!');
            error.statusCode = 422;

            throw error;
        }

        let imageUrl = req.body.image;

        if (req.file) {
            imageUrl = req.file.path;
        }

        if (!imageUrl) {
            const error = new Error('No file picked!');
            error.statusCode = 422;

            throw error;
        }

        const { postId } = req.params;
        const post = await Post.findById(postId).populate('creator');

        if (!post) {
            const error = new Error('Post with given id not found!');
            error.statusCode = 404;

            throw error;
        }

        if (post.creator._id.toString() !== req.userId.toString()) {
            const error = new Error('Not Authorized!');
            error.statusCode = 403;

            throw error;
        }

        const { title, content } = req.body;

        post.title = title;
        post.content = content;
        post.imageUrl = imageUrl;

        if (imageUrl !== post.imageUrl) {
            clearImage(post.imageUrl);
        }

        await post.save();

        io.getIO().emit('posts', {
            action: 'update',
            post
        });

        return res.status(200).json({
            message: 'Post updated!',
            post
        });
    } catch (ex) {
        if (!ex.statusCode) {
            ex.statusCode = 500;
        }

        next(ex);
    }
};

exports.deletePost = async (req, res, next) => {
    try {
        const { postId } = req.params;
        const post = await Post.findById(postId);

        if (!post) {
            const error = new Error('Post with given id not found!');
            error.statusCode = 404;

            throw error;
        }

        if (post.creator.toString() !== req.userId.toString()) {
            const error = new Error('Not Authorized!');
            error.statusCode = 403;

            throw error;
        }

        clearImage(post.imageUrl);
        await Post.findByIdAndRemove(postId);

        const user = await User.findById(req.userId);
        user.posts.pull(postId);
        await user.save();

        io.getIO().emit('posts', {
            action: 'delete',
            post: postId
        });

        return res.status(200).json({
            message: 'Post deleted.'
        });
    } catch (ex) {
        if (!ex.statusCode) {
            ex.statusCode = 500;
        }

        next(ex);
    }

};

const clearImage = filePath => {
    filePath = path.join(__dirname, '..', filePath);

    fs.unlink(filePath, err => {
        console.log(err);
    });
}