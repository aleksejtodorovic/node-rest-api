const { validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../model/User');

exports.signup = async (req, res, next) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            const error = new Error('Validation failed, entered data is incorrect!');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }


        const { email, name, password: plainPassword } = req.body;
        const password = await bcrypt.hash(plainPassword, 12);
        const user = new User({
            email,
            name,
            password
        });

        await user.save();

        res.status(201).json({
            message: 'User created',
            userId: user._id
        });
    } catch (ex) {
        if (!ex.statusCode) {
            ex.statusCode = 500;
        }

        next(ex);
    }
}

exports.login = async (req, res, next) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ email });

        if (!user) {
            const error = new Error('A user with this email could not be found!');
            error.statusCode = 401;

            throw error;
        }

        const isEqual = await bcrypt.compare(password, user.password);

        if (!isEqual) {
            const error = new Error('Wrong password!');
            error.statusCode = 401;

            throw error;
        }

        const token = jwt.sign({
            userId: user._id.toString(),
            email: user.email
        }, 'strongsecretkey', { expiresIn: '1h' });

        res.status(200).json({
            token,
            userId: user._id.toString()
        });
    } catch (ex) {
        if (!ex.statusCode) {
            ex.statusCode = 500;
        }

        next(ex);
    }
}
