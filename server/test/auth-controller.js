const chai = require('chai');
const { expect } = chai;
const sinon = require('sinon');
const sinonChai = require("sinon-chai");

chai.use(sinonChai);

const { login, signup } = require('../controllers/auth');
const User = require('../model/User');

describe('Auth Controller - Login', () => {
    let fakeNext;

    beforeEach(() => {
        fakeNext = sinon.spy();
    });

    it('should call next with error if request does not have body', async () => {
        const req = {};

        await login(req, {}, fakeNext);

        expect(fakeNext).to.have.been.calledWith(sinon.match({ statusCode: 500 }));
    });

    it('should call next with error if reading from db fails', async () => {
        const req = {
            body: {
                email: 'test@gmail.com',
                password: 'testingpass'
            }
        };

        sinon.stub(User, 'findOne')
        User.findOne.throws(new Error('Failed reading db'));

        await login(req, {}, fakeNext);

        expect(fakeNext).to.have.been.calledWith(sinon.match({ statusCode: 500 }));

        User.findOne.restore();
    });
});