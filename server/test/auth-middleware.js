const chai = require('chai');
const { expect } = chai;
const jwt = require('jsonwebtoken');
const sinon = require('sinon');
const sinonChai = require("sinon-chai");

chai.use(sinonChai);

const authMiddleware = require('../middleware/isAuth');

describe('Auth middleware', () => {
    it('should call next with exception Not Authenticated and status code 401', () => {
        const req = {
            get: () => {
                return null;
            }
        };
        const fakeNext = sinon.spy();
        authMiddleware(req, {}, fakeNext);

        expect(fakeNext).to.have.been.calledWith(sinon.match({ message: 'Not Authenticated!' }));
        expect(fakeNext).to.have.been.calledWith(sinon.match({ statusCode: 401 }));
    });

    it('should call next with error and status code 500 if verify fails', () => {
        const req = {
            get: () => {
                return 'Bearer awfaraffafa';
            }
        };

        sinon.stub(jwt, 'verify');
        jwt.verify.throws(new Error('Some error occured'));

        const fakeNext = sinon.spy();
        authMiddleware(req, {}, fakeNext);

        expect(fakeNext).to.have.been.calledWith(sinon.match({ message: 'Some error occured' }));
        expect(fakeNext).to.have.been.calledWith(sinon.match({ statusCode: 500 }));

        jwt.verify.restore();
    });

    it('should yield a userId into request after decoding token', () => {
        const req = {
            get: headerName => {
                return 'Bearer adadwwadawga';
            }
        };

        sinon.stub(jwt, 'verify');
        jwt.verify.returns({ userId: 'abc' });

        authMiddleware(req, {}, () => { });
        expect(jwt.verify.called).to.be.true;
        expect(req).to.have.property('userId', 'abc');

        jwt.verify.restore();
    });
});