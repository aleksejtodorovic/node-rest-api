const cluster = require('cluster');
const os = require('os');

if (cluster.isMaster) {
    const cpus = os.cpus().length;
    let forks = 0;

    do {
        cluster.fork();
        ++forks;
    } while (forks < cpus);
} else {
    require('./app');
}