const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const multer = require('multer');

const feedRouters = require('./routes/feed');
const authRouters = require('./routes/auth');

const app = express();

const fileStorage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, 'images');
    },
    filename: (req, file, callback) => {
        callback(null, `${new Date().toISOString()}-${file.originalname}`);
    }
});

const fileFilter = (req, file, callback) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
        callback(null, true);
    } else {
        callback(null, false);
    }
};

app.use(bodyParser.json());
app.use(multer({ storage: fileStorage, fileFilter }).single('image'));
app.use('/images', express.static(path.join(__dirname, 'images')));


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use('/feed', feedRouters);
app.use('/auth', authRouters);

app.use((error, req, res, next) => {
    const { status, data, message } = error;

    res.status(status || 500).json({
        message,
        data
    });
});

mongoose.connect('mongodb+srv://node-course:node123@apollo-graphql-zmscw.mongodb.net/rest-api?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
    .then(() => {
        const server = app.listen(4000);
        const io = require('./socket').init(server);

        io.on('connection', socket => {
            console.log('Client connected');
        });

        console.log('Started app');
    })
    .catch(ex => {
        console.log(ex);
    });