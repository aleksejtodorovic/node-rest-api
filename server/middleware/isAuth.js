const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const authHeader = req.get('Authorization');

        if (!authHeader) {
            const error = new Error('Not Authenticated!');
            error.statusCode = 401;

            throw error;
        }

        const token = req.get('Authorization').split(' ')[1]; // Get rid of Bearer - get only token
        const decodedToken = jwt.verify(token, 'strongsecretkey'); // secret used in auth.js

        if (!decodedToken) {
            const error = new Error('Not Authenticated!');
            error.statusCode = 401;

            throw error;
        }

        req.userId = decodedToken.userId;
        next();
    } catch (ex) {
        if (!ex.statusCode) {
            ex.statusCode = 500;
        }

        next(ex);
    }
}