const { Router } = require('express');
const { body } = require('express-validator');

const User = require('../model/User');

const { signup, login } = require('../controllers/auth');

const router = Router();

router.put(
    '/signup',
    [
        body('email')
            .isEmail()
            .withMessage('Please enter a valid email')
            .custom(async (value, { req }) => {
                const user = await User.findOne({ email: value });

                if (user) {
                    throw new Error('Email address already exists!');
                }
            })
            .normalizeEmail(),
        body('password')
            .trim()
            .isLength({ min: 5 }),
        body('name')
            .trim()
            .not()
            .isEmpty()
    ],
    signup
);

router.post('/login', login);

module.exports = router;