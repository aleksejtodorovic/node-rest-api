const { Router } = require('express');
const { body } = require('express-validator');

const { getPosts, getPost, postPost, putPost, deletePost } = require('../controllers/feed');
const isAuth = require('../middleware/isAuth');

const router = Router();

router.get('/posts', isAuth, getPosts);

router.post(
    '/post',
    isAuth,
    [
        body('title')
            .trim()
            .isLength({ min: 5 }),
        body('content')
            .trim()
            .isLength({ min: 5 })
    ],
    postPost
);

router.get('/post/:postId', getPost);

router.put(
    '/post/:postId',
    isAuth,
    [
        body('title')
            .trim()
            .isLength({ min: 5 }),
        body('content')
            .trim()
            .isLength({ min: 5 })
    ],
    putPost);

router.delete('/post/:postId', isAuth, deletePost);

module.exports = router;